//A. Create an array with the values (1, 2, 3, 4, 5, 6, 7) and shuffle it.

class Shuffle {
    public static void shuffleArray(Object[] array) {
        int size=array.length;
        for (int i = 0; i < size; i++) {
            
            int j = i + (int)(Math.random() * (size - i));
            Object temp = array[j];
            array[j] = array[i];
            array[i] = temp;

        }

    }

    public static void main(String args[]) {

        String[] arr = {"1","2","3","4","5","6","7"};
        Shuffle.shuffleArray(arr);
        System.out.println("Array shuffle");
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}