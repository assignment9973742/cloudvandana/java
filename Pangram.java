public class Pangram{
    public static boolean isPangram(String str){
        if(str.length()<26){
            return false;
        }else{
            for(char ch='a';ch<='z';ch++){
                if(str.indexOf(ch)<0){
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String args[]){
        String str="Pack my box with five dozen liquor jugs";
        System.out.println(str);
        boolean Pangram=isPangram(str.toLowerCase());
        if(Pangram){
            System.out.println("The given sentence is pangram");
        }else{
            System.out.println("The given sentence is not pangram");
        }



    }

}