//B. Enter a Roman Number as input and convert it to an integer. (ex IX = 9)

import java.util.Scanner;

public class RomanInteger {
     public static int result;
        public static int romanToInteger(String romanNum){
        char[] roman=romanNum.toCharArray();
        int length=roman.length;
        for(int i=length-1;i>-1;i--){
            if(i-1>-1 && roman[i]=='V' && roman[i-1]=='I'){
                result+=4;
                i--;
                continue;
            }
            else if(i-1>-1 && roman[i]=='X' && roman[i-1]=='I'){
                result+=9;
                i--;
                continue;
            }
            else if(i-1>-1 && roman[i]=='L' && roman[i-1]=='X'){
                result+=40;
                i--;
                continue;
            }
            else if(i-1>-1 && roman[i]=='C' && roman[i-1]=='X'){
                result+=90;
                i--;
                continue;
            }

            if(i>-1){
                if(roman[i]=='I'){
                    result+=1;
                }
                else if(roman[i]=='V'){
                    result+=5;
                }
                else if(roman[i]=='X'){
                    result+=10;
                }
                else if(roman[i]=='L'){
                    result+=50;
                }
                else if(roman[i]=='C'){
                    result+=100;
                }
            }
        }
        return  result;

    }
    public static void main(String argd[]){
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter Roman Number: ");
        String romanNum=sc.nextLine();
        //String romanNum="IX";
        RomanInteger.romanToInteger(romanNum);
        System.out.println("Roman Number: "+romanNum+" Integer: "+result);

        
    }
    
}
